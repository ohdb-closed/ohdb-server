/**
 * routes/me.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/
/*jslint unparam:true*/

module.exports = {
    path: '/me',
    params: {},
    get: function (req, res) {
        "use strict";

        // stringify then parse to filter out
        // unwanted crap
        var user = JSON.parse(JSON.stringify(req.user)),
            resp = {},
            i;

        // further omit properties
        // (do not transfer api pair
        //  or anything with _)
        for (i in user) {
            if (user.hasOwnProperty(i) && i !== 'api' && i !== 'password') {
                if (i[0] === '_') {
                    if (i === '_id') {
                        resp[i] = user[i];
                    }
                } else {
                    resp[i] = user[i];
                }
            }
        }

        // stringify and return
        res.end(JSON.stringify(resp));
    }
};
