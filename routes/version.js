/**
 * routes/version.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/
/*jslint unparam:true*/

module.exports = {
    path: '/version',
    params: {},
    get: function (req, res) {
        "use strict";

        var fs = require('fs'),
            path = require('path'),
            response = {
                version: 'unknown'
            };

        fs.readFile(path.resolve(__dirname, '..', 'package.json'), 'utf8', function (err, data) {
            if (err) {
                response.status = 'error';
                response.message = String(err);
            } else {
                response.status = 'success';
                response.message = 'Hello, there.';
                response.version = JSON.parse(data).version;
            }

            res.end(JSON.stringify(response));
        });
    }
};
