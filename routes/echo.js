module.exports = {
    path: '/echo/:echo',
    params: {
        post: {
            echo: String
        }
    },
    get: function (req, res) {
        "use strict";
        res.end(req.params.echo);
    },
    post: function (req, res) {
        "use strict";
        res.end(JSON.stringify(req.body));
    }
};
