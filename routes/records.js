/**
 * routes/records.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

module.exports = {
    path: '/records',

    params: {
        get: {
            patient: require('mongoose').Types.ObjectId,
            clinic: require('mongoose').Types.ObjectId,
            symptoms: Array,
            prescription: 'object',
            diagnosis: String,
            date: Date,
            started: Date
        }
    },

    get: function (req, res) {
        "use strict";

        console.log(JSON.stringify(req.body));
        req.body.patient = require('mongoose').Types.ObjectId(req.body.patient);

        var map = require('event-stream').map,
            stream = require('../lib/db.js').schemas.Record.find(req.body).stream();

        // error handling
        stream.on('error', function (err) {
            res.end('{"status":"error","message":"' + String(err) + '"}');
        });

        // pipe stringified records to the client
        stream.pipe(map(function (result, next) {
            next(null, JSON.stringify(result));
        })).pipe(res);
    }
};
