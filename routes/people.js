/**
 * routes/people.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

module.exports = {
    path: '/people',
    params: {
        get: {
            birth: Date,
            gender: String,
            height: parseFloat,
            weight: parseFloat,
            location: {
                city: String,
                state: String,
                country: String
            }
        }
    },
    get: function (req, res) {
        "use strict";

        var map = require('event-stream').map,
            stream = require('../lib/db.js').schemas.Person.find(req.body, 'birth gender height weight location.city location.state location.country').stream();

        // error handling
        stream.on('error', function (err) {
            res.end('{"status":"error","message":"' + String(err) + '"}');
        });

        // pipe stringified records to the client
        stream.pipe(map(function (result, next) {
            next(null, JSON.stringify(result));
        })).pipe(res);
    }
};
