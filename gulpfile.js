/**
 * gulpfile.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    var gulp = require('gulp'),
        beautify = require('gulp-jsbeautifier'),
        jslint = require('gulp-jslint');

    gulp.task('beautify', function () {
        return gulp.src(['*.js', '**/*.js', '!node_modules/*.js', '!node_modules/**/*.js']).pipe(beautify({
            js: {
                jslintHappy: true
            }
        })).pipe(gulp.dest('.'));
    });

    gulp.task('default', ['beautify'], function () {
        return gulp.src(['*.js', 'lib/*.js', 'routes/*.js']).pipe(jslint());
    });
}());
