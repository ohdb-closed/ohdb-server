/**
 * lib/classifier.js - ohdb-api
 * Licensed under GPL.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    var LogisticRegressionClassifier = require('natural').LogisticRegressionClassifier;

    module.exports = function (json) {
        if (typeof json === 'string') {
            json = JSON.parse(json);
        }

        return typeof json === 'object' && !Object.isEmpty(json) ? LogisticRegressionClassifier.restore(json) : new LogisticRegressionClassifier();
    };
}());