/**
 * lib/proto.js
 * Licensed under GPL.
 * Copyright (C) OHDB 2015.
 **/

(function () {
    "use strict";

    // extend string protos with colors
    require('colors');

    Array.of = function (n) {
        n = n || 0;
        var arr = [];

        while (n > 0) {
            arr.push(undefined);
            n -= 1;
        }

        return arr;
    };

    // like .keys() but with values
    Object.values = function (obj) {
        return Object.keys(obj).map(function (key) {
            return obj[key];
        });
    };

    Object.isEmpty = function (obj) {
        var i;

        for (i in obj) {
            if (obj.hasOwnProperty(i)) {
                return false;
            }
        }

        return true;
    };
}());
