/**
 * lib/https.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    var fs = require('fs'),
        net = require('net'),
        path = require('path'),
        http = require('http'),
        https = require('https'),
        ssl = {
            key: null,
            cert: null,
            ca: null
        },
        available = function (port, callback) {
            var server = net.createServer();

            server.on('error', function () {
                callback(false);
            });

            server.listen(port, function () {
                server.close();
                callback(true);
            });
        },
        nextPort = function (port, callback) {
            available(port, function (isAvailable) {
                if (isAvailable) {
                    callback(port);
                } else {
                    nextPort(port + 1, callback);
                }
            });
        };

    module.exports = function (app) {
        if (process.env.NODE_ENV === 'development') {
            fs.readFile(path.resolve(__dirname, '..', 'ssl', 'server.crt'), 'utf8', function (err, cert) {
                if (err) {
                    console.error(err);
                    process.exit(-1);
                }

                ssl.cert = cert;
                fs.readFile(path.resolve(__dirname, '..', 'ssl', 'server.key'), 'utf8', function (err, key) {
                    if (err) {
                        console.error(err);
                        process.exit(-1);
                    }

                    ssl.key = key;
                    fs.readFile(path.resolve(__dirname, '..', 'ssl', 'ca.pem'), 'utf8', function (err, ca) {
                        if (err) {
                            console.error(err);
                            process.exit(-1);
                        }

                        ssl.ca = ca;
                        console.log('* searching for open port ...');
                        nextPort(443, function (port) {
                            https.createServer(ssl, app).listen(port, function () {
                                console.log('* listening at https://localhost:%s/', port);
                            });
                        });
                    });
                });
            });
        } else {
            // heroku requires non-SSL server
            var server = http.Server(app);

            if (process.env.PORT) {
                server.listen(process.env.PORT, function () {
                    console.log('* listening at http://localhost:%s/', process.env.PORT);
                });
            } else {
                nextPort(80, function (port) {
                    server.listen(port, function () {
                        console.log('* listening at http://localhost:%s/', port);
                    });
                });
            }
        }
    };
}());
