/**
 * lib/spellcheck.js - ohdb-api
 * add delayed learning to the class
 *
 * Created by: Karim Alibhai
 * Copyright (C) 2015 OHDB.
 **/

"use strict";

var Spellcheck = require('natural/lib/natural/spellcheck/spellcheck.js'),
    metaphone = require('natural').Metaphone,
    swsc = new Spellcheck(require('natural').stopwords),
    wlist = 'abcdefghijklmnopqrstuvwxyz_0123456789',
    flatten = require('underscore').flatten;

Spellcheck.prototype.learn = function (wordlist) {
    var i;

    wordlist = wordlist || [];
    wordlist = flatten((wordlist instanceof Array ? wordlist : [wordlist]).map(function (string) {
        //console.log('learning: %s', string);
        return string.split(/\W+/g);
    })).filter(function (string) {
        return !!string;
    });

    this.trie.addStrings(wordlist);

    for (i = 0; i < wordlist.length; i += 1) {
        if (this.word2frequency.hasOwnProperty(wordlist[i])) {
            this.word2frequency[wordlist[i]] += 1;
        } else {
            this.word2frequency[wordlist[i]] = 0;
        }
    }
};

Spellcheck.prototype._getCorrections = function (word) {
    var myfixes = this.getCorrections(word),
        swfixes = swsc.getCorrections(word);

    if (swfixes.indexOf(word) !== -1 || (myfixes.length > 0 && myfixes[0] === word)) {
        return [];
    }

    return swfixes.length === 0 ? myfixes : swfixes;
};

Spellcheck.prototype.fix = function (sentence) {
    //console.log('\nfixing: %s', sentence);
    var fixes, word, i, n, result = '';

    for (i = 0, word = ''; i <= sentence.length; i += 1) {
        if (i === sentence.length || wlist.indexOf(sentence[i]) === -1) {
            if (word.trim() && !/[0-9]*/.test(word)) {
                fixes = this._getCorrections(word);

                // get best fix
                if (fixes.length > 0) {
                    // look for most phonetic match
                    for (n = 0; n <= fixes.length; n += 1) {
                        if (n === fixes.length) {
                            word = fixes[0];
                        } else if (metaphone.compare(word, fixes[n])) {
                            word = fixes[n];
                            break;
                        }
                    }
                }
            }

            result += word + (i === sentence.length ? '' : sentence[i]);
            word = '';
        } else {
            word += sentence[i];
        }
    }

    //console.log('fixed: %s', result);
    this.learn(sentence);
    return result;
};

module.exports = function (wordlist) {
    wordlist = wordlist || [];
    return new Spellcheck(wordlist);
};