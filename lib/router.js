/**
 * lib/router.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    var fs = require('fs'),
        path = require('path'),
        express = require('express'),
        router = express.Router(),
        mkRoute = function (params, next) {
            params = params || {};

            return function (req, res) {
                // verify user agent
                if (req.get('User-Agent') !== 'X-OHDB-Client') {
                    res.redirect(301, 'http://docs.ohdb.ca/');
                    res.end('Sorry, you\'re doing this wrong. You should see <a href="http://docs.ohdb.ca/">the documentation</a>.');
                } else {
                    // body is a new object, because
                    // all the unknown properties will be
                    // stripped
                    var i, body = {};

                    try {
                        // validate/cast body
                        for (i in req.body) {
                            if (req.body.hasOwnProperty(i) && params[i]) {
                                body[i] = params[i](req.body[i]);
                            }
                        }

                        // on success
                        req.body = body;
                        next(req, res);
                    } catch (e) {
                        res.end('{"status":"error","message":"' + e + '"}');
                    }
                }
            };
        },
        addRoute = function (routePath) {
            // load route config
            var config = require(routePath),
                route = router.route(config.path),
                i;

            // configure router
            for (i in config) {
                if (config.hasOwnProperty(i) && i !== 'path' && i !== 'params' && config[i]) {
                    route[i](mkRoute(config.params[i], config[i]));
                }
            }
        },
        load = function (from) {
            // resolve exact path
            from = path.resolve(__dirname, '..', 'routes', from);

            fs.readdir(from, function (err, files) {
                if (err) {
                    console.error(err);
                    process.exit(-1);
                }

                var i;

                for (i = 0; i < files.length; i += 1) {
                    if (files[i].substr(-3) === '.js') {
                        addRoute(path.resolve(__dirname, '..', 'routes', files[i]));
                    } else {
                        load(files[i]);
                    }
                }
            });
        };

    // configure with * in routes
    load('.');

    // expose configured router
    module.exports = router;
}());
