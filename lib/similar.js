/**
 * lib/similar.js
 * Licensed under GPL.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    var natural = require('natural');

    module.exports = function (stra, strb) {
        if (typeof stra === 'string') {
            stra = natural.PorterStemmer.tokenizeAndStem(stra);
        }

        if (typeof strb === 'string') {
            strb = natural.PorterStemmer.tokenizeAndStem(strb);
        }

        var x, y, matches = [];

        for (x = 0; x < stra.length; x += 1) {
            for (y = 0; y < strb.length; y += 1) {
                if (stra[x] === strb[y]) {
                    matches.push(stra[x]);

                    //if (matches.length >= (0.5 * stra.length)) {
                    //    return matches.length;
                    //}
                }
            }
        }

        return matches.length >= (0.5 * stra.length) ? matches.length : false;
    };
}());
