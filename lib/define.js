/**
 * lib/define.js - ohdb-api
 * Licensed under GPL.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    require('./proto.js');

    var request = require('request'),
        parse = require('xml2js').parseString,
        flatten = require('underscore').flatten,
        stemmify = require('natural').PorterStemmer.tokenizeAndStem,
        Definition = require('./db.js').schemas.Definition,
        base = 'http://www.dictionaryapi.com/api/v1/references/medical/xml/',
        key = process.env.DICT_API_KEY,
        define = function (word, next) {
            Definition.findOne({
                word: word
            }, function (err, defn) {
                if (err || !defn) {
                    request(base + word + '?key=' + key, function (err, res, data) {
                        if (err || !res || res.statusCode >= 400) {
                            next(err || new Error('Something went wrong: ' + (res ? res.statusCode : 'empty request object.')));
                        } else {
                            data = data.toString('utf8').replace(/\r?\n/g, '\n').replace(/\&/g, '&amp;').replace(/\-/g, '&#45;');
                            parse(data, function (err, doc) {
                                if (err) {
                                    next(err);
                                } else {
                                    doc = doc.entry_list;

                                    var defs = [],
                                        map = function (sensb) {
                                            return sensb.sens.map(function (sens) {
                                                return sens.dt.map(function (entry) {
                                                    var str = entry,
                                                        list, i, j;

                                                    if (typeof str !== 'string') {
                                                        j = 0;
                                                        list = str.d_link;
                                                        str = str._;

                                                        for (i = 1; list && j < list.length && i < str.length; i += 1) {
                                                            if (str[i] === ' ' && str[i - 1] === ' ') {
                                                                str = str.substr(0, i) + list[j] + str.substr(i);
                                                                j += 1;
                                                            }
                                                        }
                                                    }

                                                    str = str || '';

                                                    if (str[0] === ':') {
                                                        str = str.substr(1);
                                                    }

                                                    if (str[str.length - 1] === ':') {
                                                        str = str.substr(0, str.length);
                                                    }

                                                    return str.trim();
                                                }).filter(function (defn) {
                                                    return defn.indexOf(word) === -1;
                                                });
                                            });
                                        },
                                        x,
                                        y;

                                    if (doc && doc.entry) {
                                        for (x = 0; x < doc.entry.length; x += 1) {
                                            doc.entry[x].$.id = doc.entry[x].$.id.replace(/{.*?}/g, '');
                                            if (stemmify(doc.entry[x].$.id).length === 1 && stemmify(doc.entry[x].$.id)[0] === stemmify(word)[0]) {
                                                for (y = 0; y < doc.entry[x].def.length; y += 1) {
                                                    if (doc.entry[x].def[y].sensb) {
                                                        defs.push(flatten(doc.entry[x].def[y].sensb.map(map)));
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (doc && doc.suggestion) {
                                        define(doc.suggestion[0], next);
                                    } else {
                                        defs = flatten(defs);
                                        (new Definition({
                                            word: word,
                                            defns: defs
                                        })).save(function (serr) {
                                            next(serr, defs);
                                        });
                                    }
                                }
                            });
                        }
                    });
                } else {
                    next(null, defn.defns);
                }
            });
        },
        similar = function (stra, strb) {
            if (typeof stra === 'string') {
                stra = stemmify(stra);
            }

            if (typeof strb === 'string') {
                strb = stemmify(strb);
            }

            var matches = 0,
                x,
                y;

            for (x = 0; x < stra.length; x += 1) {
                for (y = 0; y < strb.length; y += 1) {
                    if (stra[x] === strb[y]) {
                        matches += 1;
                    }
                }
            }

            return matches / Math.max(stra.length, strb.length);
        };

    // definition based matching
    define.match = function (worda, wordb, next) {
        define(worda, function (err, adefs) {
            if (err) {
                next(err);
            } else {
                define(wordb, function (errb, bdefs) {
                    if (errb) {
                        next(errb);
                    } else {
                        var matrix = Array.of(adefs.length).map(function () {
                                return Array.of(bdefs.length);
                            }),
                            x,
                            y,
                            max;

                        for (x = 0; x < adefs.length; x += 1) {
                            for (y = 0; y < bdefs.length; y += 1) {
                                matrix[x][y] = similar(adefs[x], bdefs[y]);
                            }
                        }

                        for (x = 0; x < matrix.length; x += 1) {
                            max = {
                                k: 0,
                                v: matrix[x][0]
                            };

                            for (y = 1; y < matrix[x].length; y += 1) {
                                if (max.v < matrix[x][y]) {
                                    max.k = y;
                                    max.v = matrix[x][y];
                                }
                            }

                            matrix[x] = max.v;
                        }

                        next(err, Math.max.apply(Math, matrix) >= 0.5);
                    }
                });
            }
        });
    };

    module.exports = define;
}());