/**
 * lib/db.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    // make connection
    require('mongoose').connect(process.env.DB_CONN);

    // import types
    require('mongoose-type-email');

    var crypto = require('crypto'),
        bcrypt = require('bcrypt'),
        natural = require('natural'),
        mongoose = require('mongoose'),
        mversion = require('mongoose-version'),
        randomstring = require('randomstring'),
        Q = require('q'),
        Schema = mongoose.Schema,
        ObjectId = Schema.Types.ObjectId,
        db = {
            // the schemas will be stored here
            schemas: {
                User: {
                    role: String, // researcher/clinic
                    name: String, // any string for company name or researcher name is valid
                    email: mongoose.SchemaTypes.Email, // proper email required
                    password: String,
                    api: {
                        key: String,
                        secret: String
                    }
                },

                Person: {
                    // each array element is a part of the name where
                    // .length >= 2
                    // [0] = first_name
                    // [length - 1] = last_name
                    name: Array,
                    birth: Date, // date-of-birth
                    gender: String, // male/female/other (based on legal format)
                    height: 'number', // height in cm
                    weight: 'number', // weight in kg
                    location: {
                        address: String,
                        city: String,
                        state: String,
                        country: String,
                        postalCode: String,
                        latlong: Schema.Types.Mixed
                    },
                    origin: {
                        city: String,
                        state: String,
                        country: String
                    }
                },

                Record: {
                    clinic: ObjectId, // _id of User record
                    patient: ObjectId, // _id of Person record
                    //diagnostic: Schema.Types.Mixed,
                    diagnostics: Array,
                    symptoms: Array,
                    repeat: Array,
                    prescription: {
                        drug: String,
                        dosage: String
                    },
                    diagnosis: Array,
                    date: Date,
                    started: Date
                },

                Definition: {
                    word: String,
                    defns: Array
                },

                // the symptom map should be set specifically
                // with tokenized stems
                // i.e. {
                //     'name'     : 'influenza',
                //     'symptoms' : {
                //         'runny nose' : ['run',  'nose'],
                //         'dry cough'  : ['dry', 'cough'],
                //         ...
                //     }
                // }

                Disease: {
                    name: String,
                    stemmed: Array,
                    symptoms: Schema.Types.Mixed
                },

                DiseaseDetail: {
                    ofdisease: ObjectId,
                    nE: 'number',
                    nR: 'number',
                    symptoms: Schema.Types.Mixed //,
                        //bayes: Schema.Types.Mixed
                },

                DiseaseGroup: {
                    diseases: Array,
                    symptoms: Schema.Types.Mixed,
                    stems: Array
                },

                Network: {
                    ofgroup: ObjectId,
                    config: Schema.Types.Mixed,
                    isolates: 'number',
                    times: 'number'
                },

                // probability management records
                // every table name that ends with 'Info'
                // holds info on every occured event and
                // the total number of events

                AgeInfo: {
                    name: String,
                    nE: 'number',
                    events: Schema.Types.Mixed
                },

                WeightInfo: {
                    name: String,
                    nE: 'number',
                    events: Schema.Types.Mixed
                },

                SymptomsInfo: {
                    name: String,
                    nE: 'number',
                    events: Schema.Types.Mixed
                }
            },

            validate: {
                User: {
                    role: [function (role) {
                        return (['researcher', 'clinic']).indexOf(role) !== -1;
                    }, 'role must be researcher/clinic']
                },

                Person: {
                    name: [function (arr) {
                        // at least have a first and last name
                        var isValid = arr.length >= 2;

                        arr.forEach(function (item) {
                            if (typeof item !== 'string') {
                                isValid = false;
                            }
                        });

                        return isValid;
                    }, 'name is invalid'],

                    gender: [function (gender) {
                        return gender === 'male' || gender === 'female';
                    }, 'incorrect gender'],

                    location: {
                        latlong: [function (latlong) {
                            var i, resv = ['latmin', 'latmax', 'longmin', 'longmax'];

                            for (i in latlong) {
                                if (latlong.hasOwnProperty(i)) {
                                    if (resv.indexOf(i) === -1) {
                                        return false;
                                    }

                                    if (isNaN(parseFloat(latlong[i]))) {
                                        return false;
                                    }
                                }
                            }
                        }, 'latlong co-ordinates must be proper ranges']
                    }
                }
            },

            createAPIPair: function (role, user, pass) {
                var hash = crypto.createHash('md5'),
                    pair = {};

                hash.update(user + ':' + pass);

                pair.key = role + '_' + hash.digest('hex');
                hash = crypto.createHash('RSA-SHA224');

                hash.update(pair.key);
                hash.update(randomstring.generate());

                pair.secret = hash.digest('hex');

                return pair;
            },

            register: function (config) {
                // generate key/secret pair
                var pair = db.createAPIPair(config.role, config.email, config.password),
                    promise = Q.defer();

                // add to user
                config.api = {};
                config.api.key = pair.key;
                config.api.secret = pair.secret;

                // hash the password
                bcrypt.genSalt(10, function (err, salt) {
                    if (err) {
                        promise.reject(String(err));
                    } else {
                        bcrypt.hash(config.password, salt, function (err, hash) {
                            if (err) {
                                promise.reject(String(err));
                            } else {
                                // replace password with hash
                                config.password = hash;

                                // add to database
                                (new db.schemas.User(config)).save(function (err) {
                                    if (err) {
                                        promise.reject(String(err));
                                    } else {
                                        promise.resolve('added new user: ' + config.name);
                                    }
                                });
                            }
                        });
                    }
                });

                return promise.promise;
            },

            authenticate: function (key, secret) {
                var promise = Q.defer();

                db.schemas.User.findOne({
                    api: {
                        key: key,
                        secret: secret
                    }
                }, function (err, user) {
                    if (!err && user) {
                        // resolve the promise with *
                        promise.resolve(user);
                    } else {
                        promise.reject(err);
                    }
                }).exec();

                return promise.promise;
            },

            findUserById: function (id, callback) {
                db.schemas.User.findById(id, callback);
            },

            addDisease: function (gname, gsymptoms, callback) {
                var all = [],
                    symptoms = {},
                    baseInf = {
                        name: gname,
                        nE: 0,
                        events: {}
                    },
                    save = function (record) {
                        var promise = Q.defer();

                        record.save(function (err) {
                            if (err) {
                                promise.reject(err);
                            } else {
                                promise.resolve();
                            }
                        });

                        return promise;
                    },
                    info = {
                        name: gname,
                        symptoms: gsymptoms
                    },
                    p = [],
                    i;

                for (i = 0; i < info.symptoms.length; i += 1) {
                    // save each symptom as a 
                    symptoms[info.symptoms[i]] = natural.PorterStemmer.tokenizeAndStem(info.symptoms[i]);
                    all.push(symptoms[info.symptoms[i]]);
                }

                // save array with stem refs
                info.symptoms = symptoms;

                // have an array of every stem to do
                // quick look-ups by symptom stems
                info.stems = all;

                // convert to record
                info = [
                    new db.schemas.Disease(info),
                    new db.schemas.AgeInfo(baseInf),
                    new db.schemas.WeightInfo(baseInf),
                    new db.schemas.SymptomsInfo(baseInf)
                ];

                // save everything
                for (i = 0; i < info.length; i += 1) {
                    p.push(save(info[i]));
                }

                // catch all
                Q.allSettled(Q.all(p)).then(function (results) {
                    var n;

                    for (n = 0; n < results.length; n += 1) {
                        if (results[n].state !== 'fulfilled') {
                            return callback(results[n].reason);
                        }
                    }

                    callback(null, info);
                });
            }
        },
        deepValidate = function (schema, path, criteria) {
            var i;

            // use array for path,
            // but without a first element if
            // path does not exist yet
            path = path ? [path] : [];

            // validate all criterias, very
            // deeply
            for (i in criteria) {
                if (criteria.hasOwnProperty(i)) {
                    if (criteria[i] instanceof Array) {
                        schema.path(path.concat([i]).join('.')).validate.apply(schema.path(path.concat([i]).join('.')), criteria[i]);
                    } else {
                        deepValidate(schema, path.concat([i]).join('.'), criteria[i]);
                    }
                }
            }
        },
        name;

    for (name in db.schemas) {
        if (db.schemas.hasOwnProperty(name)) {
            // changeset properties
            db.schemas[name].created = Date;
            db.schemas[name].lastModified = Date;

            // create schema, add changeset handle
            db.schemas[name] = new Schema(db.schemas[name], {
                autoIndex: process.env.NODE_ENV === 'development',
                minimize: false
            });

            db.schemas[name].plugin(mversion, {
                collection: 'changeset_' + name,
                suppressVersionIncrement: false
            });

            // convert to model
            db.schemas[name] = mongoose.model(name, db.schemas[name]);
        }
    }

    // add validations
    for (name in db.validate) {
        if (db.validate.hasOwnProperty(name)) {
            deepValidate(db.schemas[name].schema, null, db.validate[name]);
        }
    }

    // export db handle
    module.exports = db;
}());
