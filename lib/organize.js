/**
 * lib/organize.js - ohdb-api
 * Licensed under GPL.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    var natural = require('natural'),
        spellcheck = require('./spellcheck.js')(),
        //similar = require('./similar.js'),
        months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
        mkstem = function (str) {
            return natural.PorterStemmer.tokenizeAndStem(String(str)).sort();
        },
        rmstops = function (str, extras, allow) {
            var i, stem, clean = '';

            extras = extras || [];
            allow = allow || [];

            str = natural.AggressiveTokenizer.prototype.tokenize(str);

            for (i = 0; i < str.length; i += 1) {
                stem = natural.PorterStemmer.stem(str[i]);
                if (allow.indexOf(stem) !== -1 || (natural.stopwords.indexOf(stem) === -1 && extras.indexOf(stem) === -1)) {
                    clean += str[i] + ' ';
                }
            }

            return clean.substr(0, clean.length - 1);
        };

    module.exports = function (comments) {
        var record = {
                symptoms: [],
                repeat: []
            },
            isDiagnosis = function (_stems) {
                return _stems.filter(function (stem) {
                    return stem.indexOf('diagn') !== -1;
                }).length > 0;
            },
            singleStem = function (str) {
                return natural.PorterStemmer.tokenizeAndStem(str)[0];
            },
            diagnosis = function (_comment) {
                var slice = function (here) {
                        // next word after 'diagn(.*)' is most likely a stop word
                        // such as 'as' or 'is'
                        if (natural.stopwords.indexOf(_comment[here]) !== -1) {
                            here += 1;
                        }

                        // assume that ',|and' does a proper separation of disease
                        // names in the provided diagnosis
                        return _comment.slice(here).join(' ').split(/,|and/g).map(function (s) {
                            return s.trim();
                        }).filter(function (s) {
                            return !!s;
                        });
                    },
                    n;

                if (_comment.indexOf('inconfident') !== -1 || _comment.indexOf('unknown') !== -1) {
                    return ['unknown'];
                }

                for (n = 0; n < _comment.length; n += 1) {
                    if (singleStem(_comment[n]).indexOf('diagn') !== -1) {
                        return slice(n + 1);
                    }
                }

                return ['unknown'];
            },
            used,
            stems,
            i,
            n;

        for (i = 0; i < comments.length; i += 1) {
            used = false;
            comments[i] = spellcheck.fix(comments[i].toLowerCase()).split(/\W+/g);
            stems = mkstem(comments[i]);

            // diagnosis?
            if (isDiagnosis(stems)) {
                // find original words of diagnosis
                record.diagnosis = diagnosis(comments[i]);
                used = true;
            }

            // prescription
            if (stems.indexOf('prescript') !== -1 || stems.indexOf('prescrib') !== -1) {
                comments[i] = rmstops(comments[i].join(' '), ['prescript', 'prescrib', 'patient'], ['take']);
                n = comments[i].indexOf('take');

                record.prescription = {
                    drug: comments[i].substr(0, n === -1 ? comments[i].length : n)
                };

                comments[i] = natural.AggressiveTokenizer.prototype.tokenize(comments[i]);
                for (n = 0; n < comments[i].length; n += 1) {
                    if (comments[i][n] === 'take') {
                        record.prescription.dosage = comments[i].slice(n + 1).join(' ');
                    }
                }

                used = true;
            }

            // search for date
            for (n = 0; n < stems.length; n += 1) {
                if (months.indexOf(stems[n]) !== -1) {
                    record.started = +(new Date(stems.slice(0, n + 1).join(' ')));
                    used = true;
                    break;

                }

            }

            // if the comment has not been used,
            // then assume it is a symptom
            if (!used) {
                record.symptoms.push(comments[i].join(' '));
                /*comments[i] = comments[i].join(' ');

                for (j = 0; j < record.symptoms.length; j += 1) {
                    if (similar(record.symptoms[j], comments[i])) {
                        j = -1;
                        record.repeat.push(comments[i]);
                        break;
                    }
                }

                if (j !== -1) {
                    record.symptoms.push(comments[i]);
                }*/
            }

        }

        return record;
    };
}());
