/**
 * lib/neuralnetwork.js
 * Licensed under GPL.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    var flatten = require('underscore').flatten,
        NeuralNetwork = require('brain').NeuralNetwork,
        zeros = function (n) {
            return Array.of(n).map(function () {
                return 0;
            });
        },
        randos = function (n) {
            return Array.of(n).map(function () {
                return Math.random() * 0.4 - 0.2;
            });
        };

    // this allows us to 'extend' the network's knowledge
    // AFTER the training period has already passed
    // it is very useful for the way OHDB handles its intelligent
    // diagnosis
    //
    // assumption: the incoming record is just as trustworthy of
    // being a lesson as all the records that have previously passed
    // through the network.
    NeuralNetwork.prototype.teach = function (data, options) {
        var inputSize, hiddenSize, outputSize, layer, error, node, i;

        // to stay consistent with brain's api design,
        // one should send a proper training object as
        // one would send to the .train() method:
        // { input : { ... } , output : { ... } }
        data = data || {};
        options = options || {};

        // extend options
        options.iterations = options.iterations || 20000;
        options.errorThresh = options.errorThresh || 0.005;
        options.learningRate = options.learningRate || this.learningRate || 0.3;

        // if this is the first lesson, do train first
        if (this.inputLookup === undefined || this.outputLookup === undefined) {
            return this.train([data], options);
        }

        // extend the lookup map
        for (i in data.input) {
            if (data.input.hasOwnProperty(i) && !this.inputLookup.hasOwnProperty(i)) {
                this.inputLookup[i] = Math.max.apply(Math, Object.values(this.inputLookup)) + 1;
            }
        }

        for (i in data.output) {
            if (data.output.hasOwnProperty(i) && !this.outputLookup.hasOwnProperty(i)) {
                this.outputLookup[i] = Math.max.apply(Math, Object.values(this.outputLookup)) + 1;
            }
        }

        // the network really loves its formatting
        data = this.formatData(data)[0];

        // change the network a bit
        inputSize = Object.keys(this.inputLookup).length;
        hiddenSize = this.hiddenSize;
        outputSize = Object.keys(this.outputLookup).length;

        // if hidden size is set by class, reset it
        if (!hiddenSize) {
            hiddenSize = [Math.max(3, Math.floor(inputSize / 2))];
        }

        // save new sizes
        this.sizes = flatten([inputSize, hiddenSize, outputSize]);
        this.outputLayer = this.sizes.length - 1;

        // resize things
        for (layer = 0; layer <= this.outputLayer; layer += 1) {
            if (this.deltas[layer].length < this.sizes[layer]) {
                this.deltas[layer].push(0);
                this.errors[layer].push(0);
                this.outputs[layer].push(0);
            }

            if (layer > 0) {
                this.biases[layer].push(Math.random() * 0.4 - 0.2);

                for (node = this.weights[layer].length; node < this.sizes[layer]; node += 1) {
                    this.weights[layer].push(randos(this.sizes[layer - 1]));
                    this.changes[layer].push(zeros(this.sizes[layer - 1]));
                }

                for (node = 0; node < this.sizes[layer]; node += 1) {
                    for (i = this.weights[layer][node].length; i < this.sizes[layer - 1]; i += 1) {
                        this.weights[layer][node].push(Math.random() * 0.4 - 0.2);
                        this.changes[layer][node].push(0);
                    }
                }
            }
        }

        // train single pattern
        for (i = 0, error = 1; i < options.iterations && error > options.errorThresh; i += 1) {
            error = this.trainPattern(data.input, data.output, options.learningRate);
        }

        return {
            error: error,
            iterations: i
        };
    };

    // constructor-less instantiation
    // with default network config
    module.exports = function (options, object) {
        var network = new NeuralNetwork(options || {});

        if (!!object) {
            try {
                object = typeof object === 'string' ? JSON.parse(object) : object;
                network.fromJSON(object);
            } catch (e) {
                object = {
                    error: e
                };
            }
        }

        return network;
    };
}());
