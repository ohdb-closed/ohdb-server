/**
 * index.js - ohdb-api
 * Licensed under GPLv3.
 * Copyright (C) 2015 OHDB.
 **/

(function () {
    "use strict";

    require('./lib/proto.js');

    var express = require('express'),
        session = require('express-session'),
        cookie = require('cookie-parser'),
        body = require('body-parser'),
        morgan = require('morgan'),
        compression = require('compression'),
        passport = require('passport'),
        MongoStore = require('connect-mongo')(session),
        BasicStrategy = require('passport-http').BasicStrategy,
        db = require('./lib/db.js'),
        app = express(),
        https = require('./lib/https.js'),
        router = require('./lib/router.js');

    // configure passport
    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        db.findUserById(id, done);
    });

    // use basic auth but with mongodb
    passport.use(new BasicStrategy(function (key, secret, done) {
        db.authenticate(key, secret).then(function (userrow) {
            done(null, userrow);
        }, function (err) {
            done(err);
        });
    }));

    // configure express middlware
    app.use(morgan('dev', {}));
    app.use(compression());
    app.use(cookie());
    app.use(session({
        resave: false,
        saveUninitialized: false,
        secret: process.env.SECRET,
        store: new MongoStore({
            // a connection should already exist after
            // importing 'lib/db.js'
            mongooseConnection: require('mongoose').connection
        })
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(body.json());

    // configure pre-api checks/defaults
    /*jslint unparam:true*/
    app.use(function (req, res, next) {
        res.set('Content-Type', 'application/json');
        next();
    });
    /*jslint unparam:false*/

    // attach routing tables at the end
    app.use(passport.authenticate('basic', {
        session: true
    }), router);

    // start server
    https(app);
}());
